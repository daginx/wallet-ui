import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import './App.css'
import { BchComponent, BtcComponent, EthComponent, HomeComponent, LtcComponent } from './components'
import MyAlert from './components/common/MyAlert'
import NavBarComponent from './components/NavBar'
import { AlertProvider } from './store/AlertProvider'

const App: React.FC = () => {
  return (
    <div className="App">
      <Router basename={process.env.PUBLIC_URL}>
        <NavBarComponent />

        <AlertProvider>
          <div className="container">
            <MyAlert />
            <Switch>
              <Route exact={true} path="/" component={HomeComponent} />
              <Route path="/btc" component={BtcComponent} />
              <Route path="/eth" component={EthComponent} />
              <Route path="/ltc" component={LtcComponent} />
              <Route path="/bch" component={BchComponent} />
            </Switch>
          </div>
        </AlertProvider>
      </Router>
    </div>
  )
}

export default App
