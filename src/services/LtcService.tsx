import axios from 'axios'
import { BigNumber } from 'bignumber.js'

const decimalDivision = new BigNumber(`1${'0'.repeat(8)}`)

class LtcService {
  private baseUrl: string = 'https://testnet.litecore.io'

  public async getBalance(address: string) {
    const response = await axios.get<any>(`${this.baseUrl}/api/addr/${address}/balance`)
    const balance = response.data.toString()
    const humanBalance = new BigNumber(balance).div(decimalDivision)
    return humanBalance.toString()
  }

  public async getUtxos(address: string) {
    const fetchUtxos = (await axios.get(`${this.baseUrl}/api/addr/${address}/utxo`)).data
    return fetchUtxos
  }

  public async pushTx(serialization: string, address?: string) {
    const sentTx = (await axios.post(`${this.baseUrl}/api/tx/send`, {
      rawtx: serialization,
    })).data
    return sentTx.txid
  }
}

export default new LtcService()
