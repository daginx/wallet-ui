import axios from 'axios'
import { toLegacyAddress } from 'bchaddrjs'
import { BigNumber } from 'bignumber.js'

const decimalDivision = new BigNumber(`1${'0'.repeat(8)}`)
const addressPrefix = 'bchtest:'

class BchService {
  private baseUrl: string = 'https://api.bitcore.io'

  public async getBalance(address: string) {
    if (address.startsWith('bchtest:')) {
      address = address.substr(addressPrefix.length)
    }
    const response = await axios.get<any>(
      `${this.baseUrl}/api/BCH/testnet/address/${address}/balance`
    )
    const balance = response.data.confirmed.toString()
    const humanBalance = new BigNumber(balance).div(decimalDivision)
    return humanBalance.toString()
  }

  public async getUtxos(address: string) {
    if (address.startsWith('bchtest:')) {
      address = address.substr(addressPrefix.length)
    }

    const fetchUtxos = (await axios.get(
      `${this.baseUrl}/api/BCH/testnet/address/${address}/?unspent=true`
    )).data

    let utxos: any = fetchUtxos.map((utxo: any) => ({
      address: toLegacyAddress(utxo.address),
      amount: new BigNumber(utxo.value).div(decimalDivision).toNumber(),
      confirmations: utxo.confirmations,
      height: utxo.mintHeight,
      satoshis: utxo.value,
      scriptPubKey: utxo.script,
      txid: utxo.mintTxid,
      vout: utxo.mintIndex,
    }))

    utxos = utxos.filter((utxo: any) => !utxo.spentTxid)

    return utxos
  }

  public async pushTx(serialization: string, address?: string) {
    const sentTx = (await axios.post(`${this.baseUrl}/api/BCH/testnet/tx/send`, {
      rawTx: serialization,
    })).data
    return sentTx.txid
  }
}

export default new BchService()
