import axios from 'axios'

export class BlockInfoService {
  public async getBalance(address: string) {
    const response = await axios.get<any>(
      `https://testnet.blockchain.info/balance?active=${address}&cors=true`
    )
    const account = response.data
    if (account && account[address]) {
      return account[address].final_balance.toString()
    }
    return '0'
  }

  public async pushTx(serialization: string, address?: string) {
    const data = new FormData()
    data.append('tx', serialization)
    await axios.post(`https://testnet.blockchain.info/pushtx?cors=true`, data)
    const txs = (await axios.get(
      `https://testnet.blockchain.info/multiaddr?active=${address}&n=1&cors=true`
    )).data.txs || [{ hash: '' }]
    return txs[0].hash
  }
}
