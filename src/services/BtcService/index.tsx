import { BigNumber } from 'bignumber.js'
import { BitcoreService } from './BitcoreService'
import { BlockCypherService } from './BlockCypherService'
import { BlockInfoService } from './BlockInfoService'

const decimalDivision = new BigNumber(`1${'0'.repeat(8)}`)

class BtcService {
  private blockCypherService = new BlockCypherService()
  private blockInfoService = new BlockInfoService()
  private bitcoreService = new BitcoreService()

  public async getBalance(address: string) {
    const service = this.getRandomService()
    const balance = await service.getBalance(address)
    const humanBalance = new BigNumber(balance).div(decimalDivision)
    return humanBalance.toString()
  }

  public async getUtxos(address: string) {
    const service = this.getRandom([this.blockCypherService, this.bitcoreService])
    return service.getUtxos(address)
  }

  public async pushTx(serialization: string, address?: string) {
    const service = this.getRandomService()
    return service.pushTx(serialization, address)
  }

  private getRandomService() {
    return this.getRandom([this.blockCypherService, this.blockInfoService, this.bitcoreService])
  }

  private getRandom(array: any[]) {
    return array[Math.floor(Math.random() * array.length)]
  }
}

export default new BtcService()
