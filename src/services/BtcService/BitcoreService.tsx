import axios from 'axios'
import { BigNumber } from 'bignumber.js'

const decimalDivision = new BigNumber(`1${'0'.repeat(8)}`)

export class BitcoreService {
  private baseUrl: string = 'https://api.bitcore.io'

  public async getBalance(address: string) {
    const response = await axios.get<any>(
      `${this.baseUrl}/api/BTC/testnet/address/${address}/balance`
    )
    const account = response.data
    if (account && account.confirmed) {
      return account.confirmed.toString()
    }
    return '0'
  }

  public async getUtxos(address: string) {
    const fetchUtxos = (await axios.get(
      `${this.baseUrl}/api/BTC/testnet/address/${address}/?unspent=true`
    )).data

    const utxos: any = fetchUtxos.map((utxo: any) => ({
      address: utxo.address,
      amount: new BigNumber(utxo.value).div(decimalDivision).toNumber(),
      confirmations: utxo.confirmations,
      height: utxo.mintHeight,
      satoshis: utxo.value,
      scriptPubKey: utxo.script,
      txid: utxo.mintTxid,
      vout: utxo.mintIndex,
    }))

    return utxos
  }

  public async pushTx(serialization: string, address?: string) {
    const sentTx = (await axios.post(`${this.baseUrl}/api/BTC/testnet/tx/send`, {
      rawTx: serialization,
    })).data.tx
    return sentTx.hash
  }
}
