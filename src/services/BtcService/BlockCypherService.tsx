import axios from 'axios'
import { BigNumber } from 'bignumber.js'

const decimalDivision = new BigNumber(`1${'0'.repeat(8)}`)

export class BlockCypherService {
  public async getBalance(address: string) {
    const response = await axios.get<any>(
      `https://api.blockcypher.com/v1/btc/test3/addrs/${address}/balance`
    )
    const account = response.data
    if (account && account.balance) {
      return account.balance.toString()
    }
    return '0'
  }

  public async getUtxos(address: string) {
    const fetchUtxos =
      (await axios.get(
        `https://api.blockcypher.com/v1/btc/test3/addrs/${address}?unspentOnly=true&includeScript=true`
      )).data.txrefs || []

    const utxos: any = fetchUtxos.map((utxo: any) => ({
      address,
      amount: new BigNumber(utxo.value).div(decimalDivision).toNumber(),
      confirmations: utxo.confirmations,
      height: utxo.block_height,
      satoshis: utxo.value,
      scriptPubKey: utxo.script,
      txid: utxo.tx_hash,
      vout: utxo.tx_output_n,
    }))

    return utxos
  }

  public async pushTx(serialization: string) {
    const sentTx = (await axios.post(`https://api.blockcypher.com/v1/btc/test3/txs/push`, {
      tx: serialization,
    })).data.tx
    return sentTx.hash
  }
}
