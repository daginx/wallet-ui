import React from 'react'
import { Alert } from 'reactstrap'
import { useAlert } from '../../store/AlertProvider'

const MyAlert: React.FC = () => {
  const { state, dispatch } = useAlert()

  const onDismiss = () => dispatch({ type: 'HIDE' })

  return (
    <Alert color={state.color} isOpen={state.visible} toggle={onDismiss}>
      <div dangerouslySetInnerHTML={{ __html: state.msg }} />
    </Alert>
  )
}

export default MyAlert
