import { BigNumber } from 'bignumber.js'
import { Transaction } from 'ethereumjs-tx'
import React from 'react'
import Web3 from 'web3'
import { IAccount } from '../../interfaces/IAccount'
import Platform from '../../layouts/Platform'

const web3 = new Web3('https://rinkeby.infura.io/v3/862ef040239b491fbe5d2c9efd414d98')
const decimalDivision = new BigNumber(`1${'0'.repeat(18)}`)

const Eth: React.FC = () => {
  const newAccount = (): IAccount => {
    const account = web3.eth.accounts.create()
    return { privateKey: account.privateKey, address: account.address }
  }

  const getAccount = (privateKey: string): IAccount => {
    privateKey = privateKey.startsWith('0x') ? privateKey : `0x${privateKey}`
    const account = web3.eth.accounts.privateKeyToAccount(privateKey)
    return { privateKey: account.privateKey, address: account.address }
  }

  const getBalance = async (address: string) => {
    const balance = await web3.eth.getBalance(address)
    const humanBalance = new BigNumber(balance).div(decimalDivision)
    return humanBalance.toString()
  }

  const transfer = async (privateKey: string, to: string, humanAmount: string): Promise<string> => {
    privateKey = privateKey.startsWith('0x') ? privateKey.substr(2) : privateKey
    const { address } = getAccount(privateKey)
    const amount = new BigNumber(humanAmount).multipliedBy(decimalDivision)
    const nonce = await web3.eth.getTransactionCount(address)
    const txData = {
      from: address,
      gasLimit: web3.utils.toHex(100000),
      gasPrice: web3.utils.toHex(10e9), // 10 Gwei,
      nonce: web3.utils.toHex(nonce),
      to,
      value: web3.utils.toHex(amount.toString()),
    }
    const transaction = new Transaction(txData, { chain: 'rinkeby' })
    transaction.sign(new Buffer(privateKey, 'hex'))
    const serializedTx = transaction.serialize().toString('hex')
    const tx = await web3.eth.sendSignedTransaction(`0x${serializedTx}`)
    return `https://rinkeby.etherscan.io/tx/${tx.transactionHash}`
  }

  return (
    <Platform
      newAccount={newAccount}
      getAccount={getAccount}
      getBalance={getBalance}
      transfer={transfer}
    />
  )
}

export default Eth
