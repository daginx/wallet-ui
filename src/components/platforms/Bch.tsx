import { toLegacyAddress } from 'bchaddrjs'
import { BigNumber } from 'bignumber.js'
import React from 'react'
import { IAccount } from '../../interfaces/IAccount'
import Platform from '../../layouts/Platform'
import BchService from '../../services/BchService'

const bch = require('bitcore-lib-cash')

const decimalDivision = new BigNumber(`1${'0'.repeat(8)}`)

const Bch: React.FC = () => {
  const newAccount = (): IAccount => {
    const pk = new bch.PrivateKey(undefined, bch.Networks.testnet)
    const wif = pk.toWIF()
    const addr = pk.toAddress()
    return { privateKey: wif, address: addr.toString() }
  }

  const getAccount = (privateKey: string): IAccount => {
    const pk = new bch.PrivateKey(privateKey, bch.Networks.testnet)
    const addr = pk.toAddress()
    return { privateKey, address: addr.toString() }
  }

  const getBalance = async (address: string) => {
    return BchService.getBalance(address)
  }

  const transfer = async (privateKey: string, to: string, humanAmount: string): Promise<string> => {
    const { address } = getAccount(privateKey)
    const fetchUtxos = await BchService.getUtxos(address)
    let total = new BigNumber(0)
    const fee = new BigNumber(40000)
    const amount = new BigNumber(humanAmount).multipliedBy(decimalDivision)
    const neededAmount = amount.plus(fee)
    const utxos = []

    for (const utxo of fetchUtxos) {
      utxos.push(utxo)
      total = total.plus(new BigNumber(utxo.value))
      if (total.gte(neededAmount)) {
        break
      }
    }
    if (total.lt(neededAmount)) {
      throw new Error(`Avaible balance is ${total}, insufficient fund`)
    }

    const transaction = new bch.Transaction()
      .from(utxos)
      .to(toLegacyAddress(to), amount.toNumber())
      .change(toLegacyAddress(address))
      .fee(fee.toNumber())
      .sign(privateKey) // temporary fix fee
    const txid = await BchService.pushTx(transaction.serialize(), address)
    return `https://blockexplorer.one/bch/testnet/tx/${txid}`
  }

  return (
    <Platform
      newAccount={newAccount}
      getAccount={getAccount}
      getBalance={getBalance}
      transfer={transfer}
    />
  )
}

export default Bch
