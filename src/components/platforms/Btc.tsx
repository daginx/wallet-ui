import { BigNumber } from 'bignumber.js'
import bitcore from 'bitcore-lib'
import React from 'react'
import { IAccount } from '../../interfaces/IAccount'
import Platform from '../../layouts/Platform'
import BtcService from '../../services/BtcService'

const decimalDivision = new BigNumber(`1${'0'.repeat(8)}`)

const Btc: React.FC = () => {
  const newAccount = (): IAccount => {
    const pk = new bitcore.PrivateKey(undefined, bitcore.Networks.testnet)
    const wif = pk.toWIF()
    const addr = pk.toAddress()
    return { privateKey: wif, address: addr.toString() }
  }

  const getAccount = (privateKey: string): IAccount => {
    const pk = new bitcore.PrivateKey(privateKey)
    const addr = pk.toAddress()
    return { privateKey, address: addr.toString() }
  }

  const getBalance = async (address: string) => {
    return BtcService.getBalance(address)
  }

  const transfer = async (privateKey: string, to: string, humanAmount: string): Promise<string> => {
    const { address } = getAccount(privateKey)
    const fetchUtxos = await BtcService.getUtxos(address)
    let total = new BigNumber(0)
    const fee = new BigNumber(20000)
    const amount = new BigNumber(humanAmount).multipliedBy(decimalDivision)
    const neededAmount = amount.plus(fee)
    const utxos = []

    for (const utxo of fetchUtxos) {
      utxos.push(utxo)
      total = total.plus(new BigNumber(utxo.value))
      if (total.gte(neededAmount)) {
        break
      }
    }
    if (total.lt(neededAmount)) {
      throw new Error(`Avaible balance is ${total}, insufficient fund`)
    }

    const transaction = new bitcore.Transaction()
      .from(utxos)
      .to(to, amount.toNumber())
      .change(address)
      .fee(fee.toNumber())
      .sign(privateKey) // temporary fix fee
    const txid = await BtcService.pushTx(transaction.serialize(), address)
    return `https://live.blockcypher.com/btc-testnet/tx/${txid}`
  }

  return (
    <Platform
      newAccount={newAccount}
      getAccount={getAccount}
      getBalance={getBalance}
      transfer={transfer}
    />
  )
}

export default Btc
