import React, { useState } from 'react'
import { Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from 'reactstrap'

const NavBarComponent: React.FC = () => {
  const publicUrl = process.env.PUBLIC_URL || ''
  const [isOpen, setIsOpen] = useState(false)
  const toggle = () => setIsOpen(!isOpen)

  return (
    <Navbar className="navbar" color="light" light={true} expand="md">
      <NavbarBrand href={`${publicUrl}/`}>Wallet UI</NavbarBrand>
      <NavbarToggler onClick={toggle} />
      <Collapse isOpen={isOpen} navbar={true}>
        <Nav className="ml-left" navbar={true}>
          <NavItem>
            <NavLink
              href={`${publicUrl}/btc`}
              active={window.location.pathname === `${publicUrl}/btc`}
            >
              BTC
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              href={`${publicUrl}/eth`}
              active={window.location.pathname === `${publicUrl}/eth`}
            >
              ETH
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              href={`${publicUrl}/ltc`}
              active={window.location.pathname === `${publicUrl}/ltc`}
            >
              LTC
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              href={`${publicUrl}/bch`}
              active={window.location.pathname === `${publicUrl}/bch`}
            >
              BCH
            </NavLink>
          </NavItem>
        </Nav>
      </Collapse>
    </Navbar>
  )
}

export default NavBarComponent
