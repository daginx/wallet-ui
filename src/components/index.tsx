import React from 'react'
import Loadable from 'react-loadable'

const Loading = () => <div>Loading...</div>

export const BtcComponent = Loadable({
  loader: () => import('./platforms/Btc'),
  loading: Loading,
})

export const EthComponent = Loadable({
  loader: () => import('./platforms/Eth'),
  loading: Loading,
})

export const LtcComponent = Loadable({
  loader: () => import('./platforms/Ltc'),
  loading: Loading,
})

export const BchComponent = Loadable({
  loader: () => import('./platforms/Bch'),
  loading: Loading,
})

export const HomeComponent = Loadable({
  loader: () => import('./Home'),
  loading: Loading,
})
