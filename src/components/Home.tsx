import React from 'react'
import { Jumbotron } from 'reactstrap'

const Home: React.FC = () => {
  return (
    <Jumbotron>
      <h1 className="display-3">Wallet UI</h1>
      <p className="lead">Wallet UI for using cryptocurrencies on testnet.</p>
    </Jumbotron>
  )
}

export default Home
