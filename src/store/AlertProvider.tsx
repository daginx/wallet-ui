import React, { createContext, ReactNode, useContext, useReducer } from 'react'

interface AlertState {
  color?: string
  msg: string
  visible?: boolean
}

interface AlertAction {
  type: 'HIDE' | 'SUCCESS' | 'ERROR'
  data?: AlertState
}

interface ContextProps {
  state: AlertState
  dispatch: React.Dispatch<AlertAction>
}

const defaultState: AlertState = {
  color: '',
  msg: '',
  visible: false,
}

function reducer(state: AlertState = defaultState, action: AlertAction) {
  const { type, data } = action
  switch (type) {
    case 'SUCCESS':
      return { ...state, ...data, visible: true, color: 'success' }
    case 'ERROR':
      return { ...state, ...data, visible: true, color: 'danger' }
    case 'HIDE':
      return { ...state, visible: false }
    default:
      return state
  }
}

const AlertContext = createContext({} as ContextProps)

export interface AlertProps {
  children?: ReactNode
}

export function AlertProvider(props: AlertProps) {
  const { children } = props
  const [state, dispatch] = useReducer(reducer, defaultState)
  const value = { state, dispatch }

  return <AlertContext.Provider value={value}>{children}</AlertContext.Provider>
}

export const useAlert = () => useContext(AlertContext)
