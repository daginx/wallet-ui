export interface IAccount {
  readonly privateKey: string
  readonly address: string
}
