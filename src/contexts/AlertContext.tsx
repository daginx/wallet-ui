import React, { createContext, ReactNode, useState } from 'react'

export const Context = createContext({})

export interface AlertProps {
  color: string
  msg: string
  visible: boolean
  children?: ReactNode
}

export const Provider = (props: AlertProps) => {
  // Initial values are obtained from the props
  const { color: initColor, msg: initMsg, visible: initVisible, children } = props

  // Use State to keep the values
  const [color, setColor] = useState<string>(initColor)
  const [msg, setMsg] = useState<string>(initMsg)
  const [visible, setVisible] = useState<boolean>(initVisible)

  // Make the context object:
  const alertContext = {
    color,
    msg,
    setColor,
    setMsg,
    setVisible,
    visible,
  }

  // pass the value in provider and return
  return <Context.Provider value={alertContext}>{children}</Context.Provider>
}

export const { Consumer } = Context
