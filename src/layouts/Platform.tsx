import { faCopy, faEye, faEyeSlash, faSpinner, faSync } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React, { useCallback, useEffect, useState } from 'react'
import {
  Button,
  ButtonGroup,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  Label,
} from 'reactstrap'
import { IAccount } from '../interfaces/IAccount'
import { useAlert } from '../store/AlertProvider'

interface PlatformProps {
  newAccount: () => IAccount
  getAccount: (privateKey: string) => IAccount
  getBalance: (address: string) => Promise<string>
  transfer: (privateKey: string, to: string, humanAmount: string) => Promise<string>
}

const Platform: React.FC<PlatformProps> = ({ newAccount, getBalance, getAccount, transfer }) => {
  const [privateKey, setPrivateKey] = useState<string>('')
  const [address, setAddress] = useState<string>('')
  const [balance, setBalance] = useState<number>(0)
  const [toAddress, setToAddress] = useState<string>('')
  const [amount, setAmount] = useState<string>('')
  const [balanceLoading, setBalanceLoading] = useState<boolean>(false)
  const [transferLoading, setTransferLoading] = useState<boolean>(false)
  const [privateKeyShow, setPrivateKeyShow] = useState<boolean>(false)
  const { dispatch } = useAlert()

  const success = (msg: string) => {
    dispatch({ type: 'SUCCESS', data: { msg } })
  }

  const error = (msg: string) => {
    dispatch({ type: 'ERROR', data: { msg } })
  }

  const register = () => {
    const account = newAccount()
    setPrivateKey(account.privateKey)
    setAddress(account.address)
  }

  const refreshBalance = useCallback(async () => {
    if (!address) {
      return setBalance(0)
    }
    setBalanceLoading(loading => !loading)
    const bl = await getBalance(address)
    setBalance(Number(bl))
    setBalanceLoading(loading => !loading)
  }, [address, getBalance])

  const login = async (e: React.FormEvent<HTMLFormElement>) => {
    try {
      e.preventDefault()
      const account = getAccount(privateKey)
      setPrivateKey(account.privateKey)
      setAddress(account.address)
    } catch (err) {
      error(err.toString())
    }
  }

  const logout = () => {
    setPrivateKey('')
    setAddress('')
    setBalance(0)
  }

  const doTransfer = async () => {
    try {
      setTransferLoading(loading => !loading)
      const txUrl = await transfer(privateKey, toAddress, amount)
      setTransferLoading(loading => !loading)
      success(`Sent: <a href="${txUrl}" className="alert-link" target="_blank">${txUrl}</a>`)
    } catch (err) {
      setTransferLoading(loading => !loading)
      error(err.toString())
    }
  }

  const copyToClipboard = (id: string, value: string) => {
    navigator.clipboard.writeText(value)
    const input = document!.getElementById(id) as HTMLInputElement
    input.select()
    success(`Copied ${id} to clipboard`)
  }

  useEffect(() => {
    refreshBalance()
  }, [address, refreshBalance])

  return (
    <Form onSubmit={login}>
      <FormGroup>
        <Label for="privatekey">Private Key</Label>
        <InputGroup>
          <Input
            type={privateKeyShow ? 'search' : 'password'}
            id="privateKey"
            placeholder="Enter Private Key"
            value={privateKey}
            onChange={e => setPrivateKey(e.target.value)}
          />
          <InputGroupAddon hidden={!privateKey} addonType="append">
            <Button onClick={() => setPrivateKeyShow(show => !show)}>
              <FontAwesomeIcon icon={privateKeyShow ? faEyeSlash : faEye} />
            </Button>
            <Button onClick={() => copyToClipboard('privateKey', privateKey)}>
              <FontAwesomeIcon icon={faCopy} />
            </Button>
          </InputGroupAddon>
        </InputGroup>
        <small className="form-text text-muted">We'll never store your private key.</small>
      </FormGroup>
      <ButtonGroup>
        <Button color="success" className="btn-space" onClick={register}>
          Register
        </Button>
        <Button color="primary" className="btn-space" type="submit">
          Login
        </Button>
        <Button color="danger" onClick={logout}>
          Logout
        </Button>
      </ButtonGroup>

      <hr />
      <FormGroup>
        <Label for="address">Address</Label>
        <InputGroup>
          <Input type="text" id="address" disabled={true} value={address} readOnly={true} />
          <InputGroupAddon addonType="append">
            <Button hidden={!address} onClick={() => copyToClipboard('address', address)}>
              <FontAwesomeIcon icon={faCopy} />
            </Button>
          </InputGroupAddon>
        </InputGroup>
      </FormGroup>
      <FormGroup>
        <Label for="balance">Balance</Label>
        <InputGroup>
          <Input type="text" id="balance" disabled={true} value={balance} readOnly={true} />
          <InputGroupAddon addonType="append">
            <Button onClick={refreshBalance} disabled={balanceLoading}>
              <FontAwesomeIcon icon={faSync} spin={balanceLoading} />
            </Button>
          </InputGroupAddon>
        </InputGroup>
      </FormGroup>

      <hr />
      <FormGroup>
        <Label for="toaddress">To Address</Label>
        <InputGroup>
          <Input
            type="text"
            id="toaddress"
            value={toAddress}
            onChange={e => setToAddress(e.target.value)}
          />
        </InputGroup>
      </FormGroup>
      <FormGroup>
        <Label for="amount">Amount</Label>
        <InputGroup>
          <Input
            type="number"
            id="amount"
            value={amount}
            onChange={e => setAmount(e.target.value)}
          />
        </InputGroup>
      </FormGroup>
      <ButtonGroup>
        <Button color="success" onClick={doTransfer} disabled={transferLoading}>
          {!transferLoading ? 'Transfer' : <FontAwesomeIcon icon={faSpinner} spin={true} />}
        </Button>
      </ButtonGroup>
    </Form>
  )
}

export default Platform
